package br.edu.up.dominio;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by GIL on 04/11/2016.
 */
@Entity
@Table(name = "pessoas", schema = "cadastro")
public class Pessoa {
    private int id;
    private String nome;
    private String rg;
    private String cpf;
    private String estadoCivil;
    private String sexo;
    private Timestamp dataNasc;
    private String localNasc;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nome")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Basic
    @Column(name = "rg")
    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    @Basic
    @Column(name = "cpf")
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Basic
    @Column(name = "estado_civil")
    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Basic
    @Column(name = "sexo")
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Basic
    @Column(name = "data_nasc")
    public Timestamp getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Timestamp dataNasc) {
        this.dataNasc = dataNasc;
    }

    @Basic
    @Column(name = "local_nasc")
    public String getLocalNasc() {
        return localNasc;
    }

    public void setLocalNasc(String localNasc) {
        this.localNasc = localNasc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pessoa pessoa = (Pessoa) o;

        if (id != pessoa.id) return false;
        if (nome != null ? !nome.equals(pessoa.nome) : pessoa.nome != null) return false;
        if (rg != null ? !rg.equals(pessoa.rg) : pessoa.rg != null) return false;
        if (cpf != null ? !cpf.equals(pessoa.cpf) : pessoa.cpf != null) return false;
        if (estadoCivil != null ? !estadoCivil.equals(pessoa.estadoCivil) : pessoa.estadoCivil != null) return false;
        if (sexo != null ? !sexo.equals(pessoa.sexo) : pessoa.sexo != null) return false;
        if (dataNasc != null ? !dataNasc.equals(pessoa.dataNasc) : pessoa.dataNasc != null) return false;
        if (localNasc != null ? !localNasc.equals(pessoa.localNasc) : pessoa.localNasc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (rg != null ? rg.hashCode() : 0);
        result = 31 * result + (cpf != null ? cpf.hashCode() : 0);
        result = 31 * result + (estadoCivil != null ? estadoCivil.hashCode() : 0);
        result = 31 * result + (sexo != null ? sexo.hashCode() : 0);
        result = 31 * result + (dataNasc != null ? dataNasc.hashCode() : 0);
        result = 31 * result + (localNasc != null ? localNasc.hashCode() : 0);
        return result;
    }
}
