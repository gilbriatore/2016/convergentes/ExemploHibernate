package br.edu.up.dominio;

import javax.persistence.*;

/**
 * Created by GIL on 04/11/2016.
 */
@Entity
@Table(name = "telefones", schema = "cadastro")
public class Telefone {
    private int id;
    private String ddd;
    private String numero;
    private String tipo;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ddd")
    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    @Basic
    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Telefone telefone = (Telefone) o;

        if (id != telefone.id) return false;
        if (ddd != null ? !ddd.equals(telefone.ddd) : telefone.ddd != null) return false;
        if (numero != null ? !numero.equals(telefone.numero) : telefone.numero != null) return false;
        if (tipo != null ? !tipo.equals(telefone.tipo) : telefone.tipo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (ddd != null ? ddd.hashCode() : 0);
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        return result;
    }
}
